package com.decerto;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@AutoConfigureMockMvc
@EnableAutoConfiguration
@EnableJpaRepositories("com.decerto")
@EntityScan("com.decerto")
@ComponentScan("com.decerto")
public class CitationITConfig {

}
