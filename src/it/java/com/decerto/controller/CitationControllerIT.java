package com.decerto.controller;

import com.decerto.CitationITConfig;
import com.decerto.domain.Citation;
import com.decerto.dtos.CitationNew;
import com.decerto.dtos.CitationUpdate;
import com.decerto.repo.CitationRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest(classes = {CitationITConfig.class})
@Transactional
public class CitationControllerIT {

    private static final String RESET_SEQUENCE_SQL = "ALTER SEQUENCE public.citations_id_seq RESTART WITH 1;";

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private MockMvc mcv;

    @Autowired
    private CitationRepository citationRepository;

    @BeforeAllMethods
    public void initialize() {
        template.execute(RESET_SEQUENCE_SQL);
        newCitations();
    }

    @Test
    public void getCitations_givenNoParams_shouldSucceed() throws Exception {
        // given
        // no params

        // when
        String response = mcv.perform(
                get("/citations")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        // then
        List<Citation> actualCitations = getCitations(response)
                .stream().sorted()
                .collect(Collectors.toList());

        assertEquals(2, actualCitations.size());

        assertEquals(1L, actualCitations.get(0).getId());
        assertEquals("Cogito ergo sum.", actualCitations.get(0).getContent());
        assertEquals("Rene", actualCitations.get(0).getFirstName());
        assertEquals("Descartes", actualCitations.get(0).getLastName());

        assertEquals(2L, actualCitations.get(1).getId());
        assertEquals("Jeżeli nie potrafisz czegoś prosto wyjaśnić - to znaczy, że niewystarczająco to rozumiesz.", actualCitations.get(1).getContent());
        assertEquals("Albert", actualCitations.get(1).getFirstName());
        assertEquals("Einstein", actualCitations.get(1).getLastName());
    }

    @Test
    public void addCitation_givenValidCitationNew_shouldSucceed() throws Exception {
        // given
        CitationNew citationNew = new CitationNew("If you dont have any shadows you're not in the light", "Lady", "Gaga");
        long beforeAdd = citationRepository.findAll().size();

        // when
        mcv.perform(
                post("/citations")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(toJson(citationNew)))
                .andExpect(status().isOk())
                .andReturn();

        // then
        assertEquals(beforeAdd + 1, citationRepository.findAll().size());
        List<Citation> citations = citationRepository.findAll();
        Citation actualCitation = citationRepository.findById(3L)
                .orElseThrow(() -> new EntityNotFoundException(format("Citation with id: %d doesn't exist.", 1L)));

        assertEquals(3L, actualCitation.getId());
        assertEquals("If you dont have any shadows you're not in the light", actualCitation.getContent());
        assertEquals("Lady", actualCitation.getFirstName());
        assertEquals("Gaga", actualCitation.getLastName());
    }


    @Test
    public void addCitation_givenCitationNewEmptyContent_shouldReturnBadRequest() throws Exception {
        // given
        CitationNew citationNew = new CitationNew("", "Lady", "Gaga");
        long beforeAdd = citationRepository.findAll().size();

        // when
        mcv.perform(
                post("/citations")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(toJson(citationNew)))
                .andExpect(status().isBadRequest())
                .andReturn();

        // then
        assertEquals(beforeAdd, citationRepository.findAll().size());
    }

    @Test
    public void addCitation_givenCitationNewNullContent_shouldReturnBadRequest() throws Exception {
        // given
        CitationNew citationNew = new CitationNew(null, "Lady", "Gaga");
        long beforeAdd = citationRepository.findAll().size();

        // when
        mcv.perform(
                post("/citations")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(toJson(citationNew)))
                .andExpect(status().isBadRequest())
                .andReturn();

        // then
        assertEquals(beforeAdd, citationRepository.findAll().size());
    }

    @Test
    public void addCitation_givenCitationNewBlankContent_shouldReturnBadRequest() throws Exception {
        // given
        CitationNew citationNew = new CitationNew(" ", "Lady", "Gaga");
        long beforeAdd = citationRepository.findAll().size();

        // when
        mcv.perform(
                post("/citations")
                        .accept(APPLICATION_JSON)
                        .contentType(APPLICATION_JSON)
                        .content(toJson(citationNew)))
                .andExpect(status().isBadRequest())
                .andReturn();

        // then
        assertEquals(beforeAdd, citationRepository.findAll().size());
    }

    @AfterAllMethods
    public void clean() {
        citationRepository.deleteAll();
    }

    private void newCitations() {
        Citation citation1 = new Citation("Cogito ergo sum.", "Rene", "Descartes");
        citationRepository.save(citation1);
        Citation citation2 = new Citation("Jeżeli nie potrafisz czegoś prosto wyjaśnić - to znaczy, że niewystarczająco to rozumiesz.", "Albert", "Einstein");
        citationRepository.save(citation2);
    }

    private List<Citation> getCitations(String response) throws JsonProcessingException {
        return mapper.readValue(response, new TypeReference<>() {
        });
    }

    public static byte[] toJson(Object object) throws IOException {
        mapper.setSerializationInclusion(NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

}
