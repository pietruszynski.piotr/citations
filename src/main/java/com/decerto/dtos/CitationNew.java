package com.decerto.dtos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CitationNew {

    public String content;

    public String firstName;

    public String lastName;

}
