package com.decerto.dtos;

import lombok.Builder;

@Builder
public class CitationDto {

    public long id;

    public String content;

    public String firstName;

    public String lastName;

}
