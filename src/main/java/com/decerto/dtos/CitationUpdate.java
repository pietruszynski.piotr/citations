package com.decerto.dtos;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CitationUpdate {

    public String content;

    public String firstName;

    public String lastName;

}
