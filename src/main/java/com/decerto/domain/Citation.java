package com.decerto.domain;

import lombok.Getter;

import javax.persistence.*;

import java.util.Objects;

import static com.decerto.validator.Validator.requireNonBlank;

@Entity
@Table(name = "citations")
@Getter
public class Citation implements Comparable<Citation> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    protected Citation() {
        // JPA
    }

    public Citation(String content, String firstName, String lastName) {
        this.content = requireNonBlank(content);
        this.firstName = requireNonBlank(firstName);
        this.lastName = requireNonBlank(lastName);
    }

    public void updateContent(String content) {
        this.content = requireNonBlank(content);
    }

    public void updateFirstName(String firstName) {
        this.firstName = requireNonBlank(firstName);
    }

    public void updateLastName(String lastName) {
        this.lastName = requireNonBlank(lastName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Citation citation = (Citation) o;
        return Objects.equals(id, citation.id) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content);
    }


    @Override
    public int compareTo(Citation other) {
        return Long.signum(getId() - other.getId());
    }
}
