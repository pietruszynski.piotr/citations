package com.decerto.services;

import com.decerto.domain.Citation;
import com.decerto.dtos.CitationUpdate;
import com.decerto.dtos.CitationNew;
import com.decerto.repo.CitationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

@Service
@Transactional
@Slf4j
public class CitationService {

    private final CitationRepository citationRepository;

    @Autowired
    public CitationService(final CitationRepository citationRepository) {
        this.citationRepository = requireNonNull(citationRepository);
    }

    public List<Citation> getCitations() {
        return citationRepository.findAll();
    }

    public void addCitation(CitationNew newCitation) {
        add(newCitation.content, newCitation.firstName, newCitation.lastName);
    }

    public void updateCitation(Long id, CitationUpdate updateCitation) {
        if (id == null) {
            log.info("Create new citation.");
            add(updateCitation.content, updateCitation.firstName, updateCitation.lastName);
        } else {
            log.info(format("Update existing citation with id: %d.", id));
            update(id, updateCitation.content, updateCitation.firstName, updateCitation.lastName);
        }
    }

    public void deleteCitation(Long id) {
        if (citationRepository.existsById(id)) {
            citationRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException(format("Citation with id: %d doesn't exist.", id));
        }
    }

    private void add(String content, String firstName, String lastName) {
        Citation citation = new Citation(content, firstName, lastName);
        citationRepository.save(citation);
    }

    private void update(Long id, String content, String firstName, String lastName) {
        Citation citation = getCitation(id);
        updateContent(content, citation);
        updateFirstName(firstName, citation);
        updateLastName(lastName, citation);
    }

    private Citation getCitation(long id) {
        return citationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(format("Citation with id: %d doesn't exist.", id)));
    }

    private void updateLastName(String lastName, Citation citation) {
        if (isBlank(lastName)) {
            citation.updateLastName(lastName);
        }
    }

    private void updateFirstName(String firstName, Citation citation) {
        if (isBlank(firstName)) {
            citation.updateFirstName(firstName);
        }
    }

    private void updateContent(String content, Citation citation) {
        if (isBlank(content)) {
            citation.updateContent(content);
        }
    }

    private boolean isBlank(String content) {
        return content != null && !content.isBlank();
    }

}
