package com.decerto.controller;

import com.decerto.domain.Citation;
import com.decerto.dtos.CitationDto;
import com.decerto.dtos.CitationUpdate;
import com.decerto.dtos.CitationNew;
import com.decerto.services.CitationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@RestController
@RequestMapping(value = "/citations")
@Slf4j
public class CitationController {

    private final CitationService citationService;

    @Autowired
    public CitationController(final CitationService citationService) {
        this.citationService = requireNonNull(citationService);
    }

    @GetMapping
    public List<CitationDto> getCitations() {
        return citationService.getCitations()
                .stream()
                .map(this::toCitationDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public void addCitation(@RequestBody CitationNew newCitation) {
        try {
            citationService.addCitation(newCitation);
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument exception happened: " + e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Citation BAD REQUEST", e);
        }
    }

    @PutMapping(value = {"/", "/{id}"})
    public void updateCitation(@PathVariable(value = "id", required = false) Long id,
                               @RequestBody CitationUpdate updateCitation) {
        try {
            citationService.updateCitation(id, updateCitation);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found exception happened: " + e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Citation NOT FOUND", e);
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument exception happened: " + e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Citation BAD REQUEST", e);
        }
    }

    @DeleteMapping("{id}")
    public void deleteCitation(@PathVariable("id") long id) {
        try {
            citationService.deleteCitation(id);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found exception happened: " + e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Citation NOT FOUND", e);
        }
    }

    private CitationDto toCitationDto(Citation c) {
        return CitationDto.builder()
                .id(c.getId())
                .content(c.getContent())
                .firstName(c.getFirstName())
                .lastName(c.getLastName())
                .build();
    }

}
