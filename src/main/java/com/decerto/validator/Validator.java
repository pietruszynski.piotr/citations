package com.decerto.validator;

public class Validator<T> {

    public static String requireNonBlank(String string) {
        if (string == null || string.isBlank()) {
            throw new IllegalArgumentException();
        }
        return string;
    }

}
