package com.decerto.services;

import com.decerto.domain.Citation;
import com.decerto.dtos.CitationNew;
import com.decerto.dtos.CitationUpdate;
import com.decerto.repo.CitationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CitationService.class})
public class CitationServiceTest {

    @Autowired
    private CitationService citationService;

    @MockBean
    private CitationRepository citationRepository;

    @Test
    public void getCitations_givenNoParams_shouldSucceed() {

        // given
        List<Citation> expectedCitations = newCitations();
        given(citationRepository.findAll())
                .willReturn(expectedCitations);

        // when
        List<Citation> actualCitations = citationService.getCitations();

        // then
        verify(citationRepository, times(1)).findAll();
        verifyNoMoreInteractions(citationRepository);
        assertEquals(expectedCitations, actualCitations);
    }

    @Test
    public void addCitation_givenValidCitationNew_shouldSucceed() {
        // given
        CitationNew citationNew = new CitationNew("If you dont have any shadows you're not in the light", "Lady", "Gaga");
        Citation citation = new Citation("If you dont have any shadows you're not in the light", "Lady", "Gaga");

        // when
        citationService.addCitation(citationNew);

        // then
        verify(citationRepository, times(1)).save(citation);
        verifyNoMoreInteractions(citationRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCitation_givenCitationNewEmptyContent_shouldThrowIAE() {
        // given
        CitationNew citationNew = new CitationNew("", "Lady", "Gaga");
        // when
        citationService.addCitation(citationNew);

        // then
        // throw IllegalArgumentException
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCitation_givenCitationNewNullContent_shouldThrowIAE() {
        // given
        CitationNew citationNew = new CitationNew(null, "Lady", "Gaga");
        // when
        citationService.addCitation(citationNew);

        // then
        // throw IllegalArgumentException
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCitation_givenCitationNewBlankContent_shouldThrowIAE() {
        // given
        CitationNew citationNew = new CitationNew(" ", "Lady", "Gaga");
        // when
        citationService.addCitation(citationNew);

        // then
        // throw IllegalArgumentException
    }

    @Test
    public void updateCitation_givenNullIdValidCitationUpdate_shouldSucceed() {

        // given
        Long id = null;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", "Lady", "Gaga");
        Citation citation = new Citation("Cogito ergo sum.", "Rene", "Descartes");

        // when
        citationService.updateCitation(id, updateCitation);

        // then
        verify(citationRepository, times(1)).save(citation);
        verifyNoMoreInteractions(citationRepository);
    }

    @Test
    public void updateCitation_givenExistingIdValidCitationUpdate_shouldSucceed() {

        // given
        Long id = 1L;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", "Lady", "Gaga");
        Citation citation = new Citation("Cogito ergo sum.", "Rene", "Descartes");
        ReflectionTestUtils.setField(citation, "id", id);
        given(citationRepository.findById(id))
                .willReturn(Optional.of(citation));


        // when
        citationService.updateCitation(id, updateCitation);

        // then
        verify(citationRepository, times(1)).findById(id);
        verifyNoMoreInteractions(citationRepository);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateCitation_givenNoExistingIdValidCitationUpdate_shouldThrowENFE() {

        // given
        Long id = 2L;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", "Lady", "Gaga");
        given(citationRepository.findById(id))
                .willReturn(Optional.empty());


        // when
        citationService.updateCitation(id, updateCitation);

        // then
        // throw EntityNotFoundException
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCitation_givenNullIdInvalidCitationUpdateEmptyFirstName_shouldThrowIAE() {

        // given
        Long id = null;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", "", "Gaga");

        // when
        citationService.updateCitation(id, updateCitation);

        // then
        // throw IllegalArgumentException
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCitation_givenNullIdInvalidCitationUpdateNullFirstName_shouldThrowIAE() {

        // given
        Long id = null;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", null, "Gaga");

        // when
        citationService.updateCitation(id, updateCitation);

        // then
        // throw IllegalArgumentException
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCitation_givenNullIdInvalidCitationUpdateBlankFirstName_shouldThrowIAE() {

        // given
        Long id = null;
        CitationUpdate updateCitation = new CitationUpdate("If you dont have any shadows you're not in the light", "", "Gaga");

        // when
        citationService.updateCitation(id, updateCitation);

        // then
        // throw IllegalArgumentException
    }

    @Test
    public void deleteCitation_givenExistingCitationId_shouldSucceed() {
        // given
        Long id = 1L;
        given(citationRepository.existsById(id))
                .willReturn(Boolean.TRUE);

        // when
        citationService.deleteCitation(id);

        // then
        verify(citationRepository, times(1))
                .existsById(id);
        verify(citationRepository, times(1))
                .deleteById(id);
        verifyNoMoreInteractions(citationRepository);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteCitation_givenNoExistingCitationId_shouldThrowENFE() {
        // given
        Long id = 2L;
        given(citationRepository.existsById(id))
                .willReturn(Boolean.FALSE);

        // when
        citationService.deleteCitation(id);

        // then
        // throw EntityNotFoundException
    }

    private List<Citation> newCitations() {
        Citation citation1 = new Citation("Cogito ergo sum.", "Rene", "Descartes");
        Citation citation2 = new Citation("Jeżeli nie potrafisz czegoś prosto wyjaśnić - to znaczy, że niewystarczająco to rozumiesz.", "Albert", "Einstein");
        return Arrays.asList(citation1, citation2);
    }

}
